import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from 'src/app/mis-clases/producto';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: Producto[];
  categoriaActual:number;
  porBusqueda:string;

  constructor(private productoService: ProductoService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      () => {
        this.listaDeproductos();
      }

    )
    
  }

  listaDeproductos(){

    const busquedaTemp = this.route.snapshot.paramMap.has('clave');
    if(busquedaTemp){
      this.cargarPorBusqueda();
    }
    else{
      this.cargarProductosPorCategorias();
    }
    
  }

  cargarProductosPorCategorias(){

    const categoriaTem = this.route.snapshot.paramMap.has('id');
    if(categoriaTem){
      this.categoriaActual = +this.route.snapshot.paramMap.get('id'); 
    }else{
      this.categoriaActual = 1;
    }
    this.productoService.getCategorias(this.categoriaActual).subscribe(
       data => {
        this.productos = data;
      })
  }

  cargarPorBusqueda() {
    this.porBusqueda = this.route.snapshot.paramMap.get('clave'); 
    this.productoService.getBusqueda(this.porBusqueda).subscribe(
      data => {
       this.productos = data;
     })
    
  }
}

