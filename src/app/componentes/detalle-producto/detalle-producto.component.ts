import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from 'src/app/mis-clases/producto';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css'],
})
export class DetalleProductoComponent implements OnInit {
  producto: Producto = new Producto();
  constructor(private productoService: ProductoService,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(() =>{
      this.cargarDetalleProducto();
    })
  }
  cargarDetalleProducto() {
    const detalleID = +this.route.snapshot.paramMap.get('id');
    this.productoService.getProducto(detalleID).subscribe(
      data =>{
        this.producto = data;

    })
  }
}
