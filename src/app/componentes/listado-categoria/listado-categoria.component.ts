import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/mis-clases/categoria';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-listado-categoria',
  templateUrl: './listado-categoria.component.html',
  styleUrls: ['./listado-categoria.component.css']
})
export class ListadoCategoriaComponent implements OnInit {

  categorias:Categoria[];

  constructor(private productoService: ProductoService) { }

  ngOnInit(): void {
    this.cargarCategorias();
  }
  cargarCategorias() {
    this.productoService.cargarCategorias().subscribe(
      data => {
        console.log("categorias" + JSON.stringify(data));
        this.categorias = data;
      });
    
    
  }

}
