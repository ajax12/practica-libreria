export class Producto {

  sku:string;
  nombre:string;
  descripcion:string;
  precioUnitario:number;
  imagenUrl:string;
  active:boolean;
  unidadEnStock:number;
  fechaCreacion:Date;
}
