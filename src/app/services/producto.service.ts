import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Producto } from '../mis-clases/producto';
import { map } from 'rxjs/operators';
import { Categoria } from '../mis-clases/categoria';

@Injectable({
  providedIn: 'root',
})
export class ProductoService {
  private baseURL = 'http://localhost:8080/api/productos';
  private categoriasURL = 'http://localhost:8080/api/categorias';
  
  constructor(private httpClient: HttpClient) {}
  
  getListaProductos(): Observable<Producto[]> {
    return this.httpClient
    .get<GetProductosRespuesta>(this.baseURL)
    .pipe(map((response) => response._embedded.productos));
  }
  
  getCategorias(idCategoria: number): Observable<Producto[]> {
    const categoriaURL = `${this.baseURL}/search/findByCategoriaId?id=${idCategoria}`;
    return this.getProductos(categoriaURL);
  }
  
  cargarCategorias(): Observable<Categoria[]> {
    return this.httpClient
    .get<GetCategoriasRespuesta>(this.categoriasURL)
    .pipe(map((response) => response._embedded.categorias));
  }
  
  getBusqueda(porBusqueda: string):Observable<Producto[]> {
    const busquedaURL = `${this.baseURL}/search/findByNombreContaining?nombre=${porBusqueda}`;
    return this.getProductos(busquedaURL);
    
  }
  
  getProducto(detalleID: number):Observable<Producto> {
    const productoURL = `${this.baseURL}/${detalleID}`;
    return this.httpClient.get<Producto>(productoURL);
    
  }

  private getProductos(busquedaURL: string): Observable<Producto[]> {
    return this.httpClient
      .get<GetProductosRespuesta>(busquedaURL)
      .pipe(map((response) => response._embedded.productos));
  }
}

interface GetProductosRespuesta {
  _embedded: {
    productos: Producto[];
  };
}

interface GetCategoriasRespuesta {
  _embedded: {
    categorias: Categoria[];
  };
}
