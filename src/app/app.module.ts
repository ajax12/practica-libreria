import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { ListadoCategoriaComponent } from './componentes/listado-categoria/listado-categoria.component';
import { ProductosComponent } from './componentes/productos/productos.component';
import { BuscadorComponent } from './componentes/buscador/buscador.component';

import { RouterModule, Routes } from '@angular/router';
import { DetalleProductoComponent } from './componentes/detalle-producto/detalle-producto.component';

export const routes: Routes = [
  { path: 'productos/:id', component: DetalleProductoComponent  },
  { path: 'buscar/:clave', component: ProductosComponent },
  { path: 'categorias/:id', component: ProductosComponent },
  { path: 'categorias', component: ProductosComponent },
  { path: 'productos', component: ProductosComponent },
  { path: '', pathMatch: 'full', redirectTo:'/productos' },
  { path: '**', pathMatch: 'full', redirectTo: '/productos' },
]

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    ListadoCategoriaComponent,
    ProductosComponent,
    BuscadorComponent,
    DetalleProductoComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
